#!/bin/sh

git config --global user.name "Eduardo Lavaque"
git config --global user.email "me@greduan.com"
git config --global init.defaultBranch main
#git config --global commit.gpgsign true
git config --global commit.verbose true
git config --global push.followTags true
git config --global core.editor nvim
git config --global core.excludesFile ~/.gitignore
git config --global core.pager "less -R"
git config --global core.ignorecase false
git config --global help.autocorrect 1
git config --global color.ui true
git config --global core.eol lf
git config --global core.autocrlf input
git config --global merge.conflictstyle diff3
git config --global merge.tool vimdiff
git config --global mergetool.prompt false
git config --global diff.algorithm patience
git config --global diff.compactionHeuristic true
git config --global alias.cob 'checkout -b'
git config --global alias.me '!git merge $(git branch | cut -c 3- | uniq -u | fzf)'
git config --global alias.mer '!git merge $(git branch -r | cut -c 3- | sed "s/\s.*//" | uniq -u | fzf)'
git config --global alias.co '!git checkout $(git branch | cut -c 3- | uniq -u | fzf)'
git config --global alias.cor '!git checkout -t $(git branch -r | cut -c 3- | uniq -u | fzf)'
git config --global alias.diffs 'diff --staged'
git config --global alias.st 'status -sb -uall'
git config --global alias.lo 'log --graph --pretty=format:"%C(yellow)%h  %Cblue%ad  %<(7)%Cgreen%an  %Cred%d %Creset%s" --date=short'
git config --global alias.dis 'checkout --'
git config --global alias.re 'reset HEAD'
git config --global alias.a '!git add $(git status -s | cut -d\  -f3 | fzf)'
git config --global alias.cobb '!f(){ git branch -D $1 && git checkout -b $1; };f'
git config --global push.default simple
git config --global github.user greduan
git config --global merge.conflictstyle zdiff3
git config --global pull.ff only
git config --global rerere.enabled true
git config --global transfer.fsckobjects true
git config --global fetch.fsckobjects true
git config --global receive.fsckObjects true
git config --global log.date iso
