vim.g.mapleader = ' '

vim.keymap.set('n', 'J', 'mzJ`z') -- join
vim.keymap.set({'n', 'v'}, '<leader>y', [["+y]]) -- yank to OS clipboard
vim.keymap.set('n', '<leader>Y', [["+Y]]) -- yank to OS clipboard
vim.keymap.set('n', '*', '*N') -- stay on current match

