require('greduan.set')
require('greduan.map')
require('greduan.autocmd')

local function bootstrap_pckr()
  local pckr_path = vim.fn.stdpath('data') .. '/pckr/pckr.nvim'

  if not vim.uv.fs_stat(pckr_path) then
    vim.fn.system({
      'git',
      'clone',
      '--filter=blob:none',
      'https://github.com/lewis6991/pckr.nvim',
      pckr_path
    })
  end

  vim.opt.rtp:prepend(pckr_path)
end

bootstrap_pckr()

local cmd = require('pckr.loader.cmd')
local keys = require('pckr.loader.keys')

require('pckr').add{
-- motions, operators, text objects
{ 'machakann/vim-sandwich' }, -- surround

-- lang support
{ 'Tetralux/odin.vim' },
{ 'joerdav/templ.vim' },

-- editor
{ 'jiangmiao/auto-pairs', config_pre = function()
  vim.g.AutoPairsMultilineClose = 0
end },
{ 'tpope/vim-rsi' }, -- readline key bindings
{ 'editorconfig/editorconfig-vim', config_pre = function()
  if (vim.fn.has('mac') == 1) then
    vim.g.EditorConfig_exec_path = '/opt/homebrew/bin/editorconfig'
  else
    vim.g.EditorConfig_exec_path = '/usr/local/bin/editorconfig'
  end
end },
{ 'reedes/vim-pencil', config_pre = function()
  vim.g['pencil#textwidth'] = 80
end },

-- navigation
{ 'justinmk/vim-dirvish' },
}
