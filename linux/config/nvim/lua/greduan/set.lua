-- performance
vim.opt.lazyredraw = true

-- search
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true
vim.opt.hlsearch = true

-- ui
vim.opt.number = true
vim.opt.termguicolors = true
vim.opt.sidescrolloff = 30
vim.opt.scrolloff = 20
vim.opt.isfname:append('@-@')

-- navigation
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.hidden = true -- don't forget buffers that aren't visible

-- misc.
vim.opt.autoread = true
