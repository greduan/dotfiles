;;; init.el --- Entrypoint to Emacs config
;;; Commentary:
;;; Code:
(load-file (expand-file-name "~/Code/emacs.onboard/onboard.el"))

(provide 'init)
;;; init.el ends here
